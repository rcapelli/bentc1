﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Grupo1.AgendaDeTurnos.Migrations
{
    public partial class prueba5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Centros_Direcciones_DireccionId",
                table: "Centros");

            migrationBuilder.DropForeignKey(
                name: "FK_Direcciones_Usuario_IdUsuario",
                table: "Direcciones");

            migrationBuilder.DropIndex(
                name: "IX_Direcciones_IdUsuario",
                table: "Direcciones");

            migrationBuilder.DropIndex(
                name: "IX_Centros_DireccionId",
                table: "Centros");

            migrationBuilder.DropColumn(
                name: "DireccionId",
                table: "Centros");

            migrationBuilder.RenameColumn(
                name: "IdUsuario",
                table: "Direcciones",
                newName: "IdCentro");

            migrationBuilder.CreateIndex(
                name: "IX_Direcciones_IdCentro",
                table: "Direcciones",
                column: "IdCentro",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Direcciones_Centros_IdCentro",
                table: "Direcciones",
                column: "IdCentro",
                principalTable: "Centros",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Direcciones_Centros_IdCentro",
                table: "Direcciones");

            migrationBuilder.DropIndex(
                name: "IX_Direcciones_IdCentro",
                table: "Direcciones");

            migrationBuilder.RenameColumn(
                name: "IdCentro",
                table: "Direcciones",
                newName: "IdUsuario");

            migrationBuilder.AddColumn<int>(
                name: "DireccionId",
                table: "Centros",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Direcciones_IdUsuario",
                table: "Direcciones",
                column: "IdUsuario");

            migrationBuilder.CreateIndex(
                name: "IX_Centros_DireccionId",
                table: "Centros",
                column: "DireccionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Centros_Direcciones_DireccionId",
                table: "Centros",
                column: "DireccionId",
                principalTable: "Direcciones",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Direcciones_Usuario_IdUsuario",
                table: "Direcciones",
                column: "IdUsuario",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
