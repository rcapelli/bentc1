﻿using Grupo1.AgendaDeTurnos.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Text.RegularExpressions;

namespace Grupo1.AgendaDeTurnos.Extensions
{
    public static class ValidarPassword
    {
        public static void ValidarPW(this string password, ModelStateDictionary ModelState)
        {
            if (string.IsNullOrEmpty(password))
            {
                ModelState.AddModelError(nameof(Usuario.Password), "La contraseña es requerida");
            }

            if (password.Length < 8)
            {
                ModelState.AddModelError(nameof(Usuario.Password), "La contraseña debe ser mayor a 8 caracteres");
            }

            bool contieneUnNumero = new Regex("[0-9]").Match(password).Success;
            bool contieneUnaMinuscula = new Regex("[a-z]").Match(password).Success;
            bool contieneUnaMayuscula = new Regex("[A-Z]").Match(password).Success;

            if (!contieneUnNumero || !contieneUnaMinuscula || !contieneUnaMayuscula)
            {
                ModelState.AddModelError(nameof(Usuario.Password), "La contraseña debe contener al menos una minuscula, una mayuscula y un numero");
            }
        }

    }
}