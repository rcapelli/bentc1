﻿using Grupo1.AgendaDeTurnos.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grupo1.AgendaDeTurnos.Extensions
{
    public static class VerificarUsuario
    {
        public static bool VerificarExistenciaDeUsuario(this string usuario, AgendaDeTurnosDbContext _context)
        {
            if (_context.Profesionales.Any(p => p.Username.Equals(usuario))
                || _context.Pacientes.Any(p => p.Username.Equals(usuario))
                || _context.Administradores.Any(a => a.Username.Equals(usuario)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}