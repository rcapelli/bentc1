﻿using Grupo1.AgendaDeTurnos.Database;
using Grupo1.AgendaDeTurnos.Extensions;
using Grupo1.AgendaDeTurnos.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Grupo1.AgendaDeTurnos.Controllers
{

    public class PacientesController : Controller
    {
        private readonly AgendaDeTurnosDbContext _context;

        public PacientesController(AgendaDeTurnosDbContext context)
        {
            _context = context;
        }

        /* --------------------- INDEX --------------------- */
        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Pacientes.ToListAsync());
        }

        /* --------------------- DETAILS --------------------- */

        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paciente = await _context.Pacientes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (paciente == null)
            {
                return NotFound();
            }

            return View(paciente);
        }

        /* --------------------- CREACION --------------------- */

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Create([Bind("Id,Nombre,Apellido,Dni,Rol,Username")] Paciente paciente, string password)
        {
            string username = paciente.Username.ToUpper();

            if (username.VerificarExistenciaDeUsuario(_context))
            {
                ViewBag.Error = "El usuario ya existe";
                return View(paciente);
            }

            password.ValidarPW(ModelState);

            paciente.Rol = RolesEnum.CLIENTE;
            if (ModelState.IsValid)
            {
                paciente.Password = password.Encriptar();

                _context.Add(paciente);
                await _context.SaveChangesAsync();
                System.Security.Claims.ClaimsPrincipal currentUser = this.User;
                if (currentUser.IsInRole("ADMINISTRADOR"))
                {
                    return RedirectToAction(nameof(Index));
                }

                return RedirectToAction(nameof(HomeController.Index), "Home");


            }
            return View(paciente);
        }


        /* --------------------- DELETE --------------------- */

        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paciente = await _context.Pacientes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (paciente == null)
            {
                return NotFound();
            }

            return View(paciente);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var paciente = await _context.Pacientes.FindAsync(id);
            _context.Pacientes.Remove(paciente);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /* --------------------- EDICION --------------------- */
        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var paciente = await _context.Pacientes.FindAsync(id);
            if (paciente == null)
            {
                return NotFound();
            }
            return View(paciente);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public IActionResult Edit(int id, [Bind("Id,Nombre,Apellido,Dni")] Paciente paciente, string password)
        {
            EditarPaciente(id, paciente, password);

            return RedirectToAction(nameof(Index));
        }

        /* --------------------- EDICION PROPIA --------------------- */

        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.CLIENTE))]
        public IActionResult EditMe()
        {
            int.TryParse(User.FindFirstValue(ClaimTypes.NameIdentifier), out int id);
            var cliente = _context.Pacientes.Find(id);
            if (cliente == null)
            {
                return NotFound();
            }
            return View(cliente);
        }

        [HttpPost]
        [Authorize(Roles = nameof(RolesEnum.CLIENTE))]
        [ValidateAntiForgeryToken]
        public IActionResult EditMe([Bind("Dni,FechaDeNacimiento,Id,Nombre,Apellido")] Paciente paciente, string password)
        {
            int.TryParse(User.FindFirstValue(ClaimTypes.NameIdentifier), out int id);
            
            return EditarPaciente(id, paciente, password);
        }
        private IActionResult EditarPaciente(int id, Paciente paciente, string password)
        {
            if (paciente.Id != id)
            {
                return NotFound();
            }

            password.ValidarPW(ModelState);

            ModelState.Remove(nameof(Paciente.Username));

            if (ModelState.IsValid)
            {
                try
                {
                    Paciente pacienteDb = _context.Pacientes.Find(paciente.Id);
                    pacienteDb.Dni = paciente.Dni;
                    pacienteDb.Nombre = paciente.Nombre;
                    pacienteDb.Apellido = paciente.Apellido;
                    pacienteDb.Password = password.Encriptar();

                    _context.Update(pacienteDb);
                    _context.SaveChanges();

                    if (User.IsInRole(nameof(RolesEnum.ADMINISTRADOR)))
                    {
                        return RedirectToAction(nameof(Index));
                    }

                    return RedirectToAction(nameof(HomeController.Index), "Home");

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PacienteExists(paciente.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

            }

            return View(paciente);
        }

        private bool PacienteExists(int id)
        {
            return _context.Pacientes.Any(e => e.Id == id);
        }


    }
}
