﻿using Grupo1.AgendaDeTurnos.Database;
using Grupo1.AgendaDeTurnos.Extensions;
using Grupo1.AgendaDeTurnos.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Grupo1.AgendaDeTurnos.Controllers
{

    public class HomeController : Controller
    {
        private readonly AgendaDeTurnosDbContext _context;
        public HomeController(AgendaDeTurnosDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {

            if (!_context.Administradores.Any())
            {
                Seed();
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void Seed()
        {
            var dire = new Direccion
            {
                Calle = "Aristobulo del valle2",
                Altura = 285,
                Localidad = "CABA",
                Provincia = "BUENOS AIRES"
            };

            var centro1 = new Centro
            {
                Direccion = dire,
                Nombre = "The best centro",
            };



            var tel = new Telefono
            {
                NumeroCelular = "111122222",
                TelefonoAlternativo = "456456456"
            };

            var mail = new Mail
            {
                Descripcion = "abc@abc.com"
            };



            var tel2 = new Telefono
            {
                NumeroCelular = "111122222",
                TelefonoAlternativo = "456456456"
            };

            var mail2 = new Mail
            {
                Descripcion = "abc@abc.com"
            };



            var prestacion = new Prestacion
            {
                Nombre = "Odontologia General",
                Monto = 500,
                DuracionHoras = 1
            };

            var profesional = new Profesional
            {
                Nombre = "prof",
                Apellido = "prof",
                Centro = centro1,
                Dni = "12312312",
                Prestacion = prestacion,
                Rol = RolesEnum.PROFESIONAL,
                Mails = new List<Mail>(),
                Turnos = new List<Turno>(),
                Telefonos = new List<Telefono>(),
                Disponibilidades = new List<Disponibilidad>(),
                Username = "prof",
                Password = "Profesional123".Encriptar()
            };

            var disponibilidad = new Disponibilidad(9, 23, DiasEnum.Sabado);
            
            profesional.Disponibilidades.Add(disponibilidad);

            var paciente = new Paciente
            {
                Nombre = "paciente",
                Apellido = "paciente",
                Dni = "12312312",
                Rol = RolesEnum.CLIENTE,
                Mails = new List<Mail>(),
                Telefonos = new List<Telefono>(),
                Turnos = new List<Turno>(),
                Username = "paciente",  
                Password = "Paciente123".Encriptar()
            };

            Administrador admin = new Administrador()
            {
                Nombre = "admin",
                Apellido = "admin",
                Dni = "12312312",
                Rol = RolesEnum.ADMINISTRADOR,
                Username = "admin",
                Password = "admin".Encriptar()
            };

            profesional.Mails.Add(mail);
            profesional.Telefonos.Add(tel);            

            paciente.Telefonos.Add(tel2);
            paciente.Mails.Add(mail2);

            _context.Administradores.Add(admin);
            _context.Pacientes.Add(paciente);
            _context.Profesionales.Add(profesional);
            _context.SaveChanges();
        }
        
    }
}
