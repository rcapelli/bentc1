﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Grupo1.AgendaDeTurnos.Database;
using Grupo1.AgendaDeTurnos.Models;
using Grupo1.AgendaDeTurnos.Extensions;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Grupo1.AgendaDeTurnos.Controllers
{
    public class Profesionales : Controller
    {
        private readonly AgendaDeTurnosDbContext _context;

        public Profesionales(AgendaDeTurnosDbContext context)
        {
            _context = context;
        }


        /* --------------------- INDEX --------------------- */
        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public async Task<IActionResult> Index()
        {
            var agendaDeTurnosDbContext = _context.Profesionales
                .Include(p => p.Centro)
                .Include(p => p.Prestacion)
                .Include(p => p.Turnos);
            var lista = _context.Profesionales.ToList();
            return View(await agendaDeTurnosDbContext.ToListAsync());
        }


        /* --------------------- DETAILS --------------------- */

        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesional = await _context.Profesionales
                .Include(p => p.Centro)
                .Include(p => p.Prestacion)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (profesional == null)
            {
                return NotFound();
            }

            return View(profesional);
        }

        /* --------------------- CREACION --------------------- */

        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public IActionResult Create()
        {
            List<Disponibilidad> disponibilidades = _context.Disponibilidades.Where(d => d.IdProfesional == 0).ToList();

            ViewData["Disponibilidades"] = new MultiSelectList(disponibilidades, "Id", "Descripcion");
            
            ViewData["DiasSemana"] = new SelectList(Enum.GetValues(typeof(DiasEnum)));
            
            ViewData["IdCentro"] = new SelectList(_context.Centros, "Id", "Nombre");
            
            ViewData["IdPrestacion"] = new SelectList(_context.Prestaciones, "Id", "Nombre");
            if (disponibilidades.Count != 0)
            {
                ViewBag.estaLleno = "1";
            }
            return View();
        }

        
        [HttpPost]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("IdPrestacion,IdCentro,Id,Nombre,Apellido,Dni,Rol,Username")] Profesional profesional, string password)
        {
            
            string username = profesional.Username.ToUpper();

            ViewData["DiasSemana"] = new SelectList(Enum.GetValues(typeof(DiasEnum)));
            ViewData["Disponibilidades"] = new SelectList(_context.Disponibilidades, "Id", "Nombre", profesional.Disponibilidades);
            ViewData["IdCentro"] = new SelectList(_context.Centros, "Id", "Nombre", profesional.IdCentro);
            ViewData["IdPrestacion"] = new SelectList(_context.Prestaciones, "Id", "Nombre", profesional.IdPrestacion);
            
            if (username.VerificarExistenciaDeUsuario(_context))
            {
                ViewBag.Error = "El usuario ya existe";
                return View(profesional);
            }

            password.ValidarPW(ModelState);

            if (ModelState.IsValid)
            {
                profesional.Username = profesional.Username.ToUpper();
                profesional.Password = password.Encriptar();
                profesional.Rol = RolesEnum.PROFESIONAL;
                profesional.Disponibilidades = new List<Disponibilidad>();

                List<Disponibilidad> listaDias = _context.Disponibilidades.Where(x=>x.IdProfesional == 0).ToList();
                foreach (Disponibilidad index in listaDias)
                {
                    profesional.Disponibilidades.Add(index);
                }
                _context.Profesionales.Add(profesional);
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));


            }
            return View(profesional);
        }

        /* --------------------- Disponibilidades del prof --------------------- */
        //[HttpGet]
        //[Authorize(Roles = nameof(RolesEnum.PROFESIONAL))]
        //public async Task<IActionResult> Disponibilidades()
        //{
        //    int profesionalId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

        //    var profesional = await _context.Profesionales
        //        .Include(p => p.Disponibilidades)
        //        .Where(p => p.Id == profesionalId)
        //        .SingleOrDefaultAsync();
        //    if (profesional == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["DiasSemana"] = new SelectList(Enum.GetValues(typeof(DiasEnum)));
        //    ViewData["Disponibilidades"] = new MultiSelectList(_context.Disponibilidades.Where(d => d.IdProfesional == 0 || d.IdProfesional == profesionalId),
        //        "Id", "Descripcion",
        //        profesional.Disponibilidades.Select(d => d.Id).ToList());
        //    return View(profesional);
        //}
        //public async Task<IActionResult> Disponibilidades(List<int> nuevasDisponibilidades)
        //{
        //    int profesionalId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

        //    var profesional = await _context.Profesionales
        //        .Include(p => p.Disponibilidades)
        //        .Where(p => p.Id == profesionalId)
        //        .SingleOrDefaultAsync();
        //    if (profesional == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["DiasSemana"] = new SelectList(Enum.GetValues(typeof(DiasEnum)));
        //    ViewData["Disponibilidades"] = new MultiSelectList(_context.Disponibilidades.Where(d => d.IdProfesional == 0 || d.IdProfesional == profesionalId),
        //        "Id", "Descripcion",
        //        profesional.Disponibilidades.Select(d => d.Id).ToList());
        //    return View(profesional);
        //}


        /* --------------------- DELETE --------------------- */

        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesional = await _context.Profesionales
                .Include(p => p.Centro)
                .Include(p => p.Prestacion)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (profesional == null)
            {
                return NotFound();
            }

            return View(profesional);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var profesional = await _context.Profesionales.FindAsync(id);
            _context.Profesionales.Remove(profesional);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /* --------------------- EDICION --------------------- */

        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesional = await _context.Profesionales.FindAsync(id);
            if (profesional == null)
            {
                return NotFound();
            }
            ViewData["IdCentro"] = new SelectList(_context.Centros, "Id", "Nombre", profesional.IdCentro);
            ViewData["IdPrestacion"] = new SelectList(_context.Prestaciones, "Id", "Nombre", profesional.IdPrestacion);
            return View(profesional);
        }

        [HttpPost]
        [Authorize(Roles = nameof(RolesEnum.ADMINISTRADOR))]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("IdPrestacion,IdCentro,Id,Nombre,Apellido,Dni")] Profesional profesional, string password)
        {
            EditarProfesional(id, profesional, password);

            return RedirectToAction(nameof(Index));
        }

        /* --------------------- EDICION PROPIA--------------------- */

        [HttpGet]
        [Authorize(Roles = nameof(RolesEnum.PROFESIONAL))]
        public IActionResult EditMe()
        {

            int.TryParse(User.FindFirstValue(ClaimTypes.NameIdentifier), out int id);
            var profesional = _context.Profesionales.Find(id);
            if (profesional == null)
            {
                return NotFound();
            }
            ViewData["IdCentro"] = new SelectList(_context.Centros, "Id", "Nombre", profesional.IdCentro);
            ViewData["IdPrestacion"] = new SelectList(_context.Prestaciones, "Id", "Nombre", profesional.IdPrestacion);
            return View(profesional);
        }

        [HttpPost]
        [Authorize(Roles = nameof(RolesEnum.PROFESIONAL))]
        [ValidateAntiForgeryToken]
        public IActionResult EditMe([Bind("IdPrestacion, IdCentro, Id, Nombre, Apellido, Dni")] Profesional profesional, string password)
        {
            int.TryParse(User.FindFirstValue(ClaimTypes.NameIdentifier), out int id);
            return EditarProfesional(id, profesional, password);
        }

        private IActionResult EditarProfesional(int id, Profesional profesional, string password)
        {
            if (profesional.Id != id)
            {
                return NotFound();
            }

            password.ValidarPW(ModelState);

            ModelState.Remove(nameof(Paciente.Username));

            if (ModelState.IsValid)
            {
                try
                {
                    Profesional profesionalDb = _context.Profesionales.Find(profesional.Id);
                    profesionalDb.Nombre = profesional.Nombre;
                    profesionalDb.Apellido = profesional.Apellido;
                    profesionalDb.Dni = profesional.Dni;
                    profesionalDb.Password = password.Encriptar();

                    _context.Update(profesionalDb);
                    _context.SaveChanges();

                    if (User.IsInRole(nameof(RolesEnum.ADMINISTRADOR)))
                    {
                        return RedirectToAction(nameof(Index));
                    }

                    return RedirectToAction(nameof(HomeController.Index), "Home");

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProfesionalExists(profesional.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

            }
            ViewData["IdCentro"] = new SelectList(_context.Centros, "Id", "Nombre", profesional.IdCentro);
            ViewData["IdPrestacion"] = new SelectList(_context.Prestaciones, "Id", "Nombre", profesional.IdPrestacion);

            return View(profesional);
        }

        private bool ProfesionalExists(int id)
        {
            return _context.Profesionales.Any(e => e.Id == id);
        }
    }
}