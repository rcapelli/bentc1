#pragma checksum "C:\Users\Ricardo\Desktop\NT\bentc1\Grupo1.AgendaDeTurnos\Views\Cuentas\Ingresar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dd41ed49a0f897e8a72862dba8e0e83c64f2bdba"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Cuentas_Ingresar), @"mvc.1.0.view", @"/Views/Cuentas/Ingresar.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Cuentas/Ingresar.cshtml", typeof(AspNetCore.Views_Cuentas_Ingresar))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Ricardo\Desktop\NT\bentc1\Grupo1.AgendaDeTurnos\Views\_ViewImports.cshtml"
using Grupo1.AgendaDeTurnos;

#line default
#line hidden
#line 2 "C:\Users\Ricardo\Desktop\NT\bentc1\Grupo1.AgendaDeTurnos\Views\_ViewImports.cshtml"
using Grupo1.AgendaDeTurnos.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dd41ed49a0f897e8a72862dba8e0e83c64f2bdba", @"/Views/Cuentas/Ingresar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4fe00df73e5f48b0ff460a680b0e5a2a113c921f", @"/Views/_ViewImports.cshtml")]
    public class Views_Cuentas_Ingresar : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Cuentas", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Ingresar", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\Ricardo\Desktop\NT\bentc1\Grupo1.AgendaDeTurnos\Views\Cuentas\Ingresar.cshtml"
  
    ViewData["Title"] = "Ingresar";

#line default
#line hidden
            BeginContext(44, 37, true);
            WriteLiteral("\r\n<div class=\"text-center\">\r\n    <h1>");
            EndContext();
            BeginContext(83, 29, false);
#line 6 "C:\Users\Ricardo\Desktop\NT\bentc1\Grupo1.AgendaDeTurnos\Views\Cuentas\Ingresar.cshtml"
    Write($"{ this.ViewData["Title"] }");

#line default
#line hidden
            EndContext();
            BeginContext(113, 117, true);
            WriteLiteral(" </h1>\r\n</div>\r\n\r\n\r\n\r\n\r\n<div class=\"row bounceIn bounceInLeft\">\r\n    <div class=\"col-lg-offset-4 col-md-4\">\r\n        ");
            EndContext();
            BeginContext(230, 884, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "dd41ed49a0f897e8a72862dba8e0e83c64f2bdba4984", async() => {
                BeginContext(297, 36, true);
                WriteLiteral("\r\n\r\n            <input type=\"hidden\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 333, "\"", 359, 1);
#line 16 "C:\Users\Ricardo\Desktop\NT\bentc1\Grupo1.AgendaDeTurnos\Views\Cuentas\Ingresar.cshtml"
WriteAttributeValue("", 341, ViewBag.ReturnUrl, 341, 18, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(360, 77, true);
                WriteLiteral(" name=\"returnUrl\" />\r\n            <div class=\"text-danger\">\r\n                ");
                EndContext();
                BeginContext(438, 13, false);
#line 18 "C:\Users\Ricardo\Desktop\NT\bentc1\Grupo1.AgendaDeTurnos\Views\Cuentas\Ingresar.cshtml"
           Write(ViewBag.Error);

#line default
#line hidden
                EndContext();
                BeginContext(451, 225, true);
                WriteLiteral("\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label laber-for=\"username\" class=\"control-label\">Nombre de usuario:</label>\r\n                <input name=\"username\" class=\"form-control\" type=\"text\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 676, "\"", 701, 1);
#line 22 "C:\Users\Ricardo\Desktop\NT\bentc1\Grupo1.AgendaDeTurnos\Views\Cuentas\Ingresar.cshtml"
WriteAttributeValue("", 684, ViewBag.Username, 684, 17, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(702, 405, true);
                WriteLiteral(@" />
            </div>

            <div class=""form-group"">
                <label label-for=""Password"" class=""control-label"">Contraseña:</label>
                <input name=""password"" class=""form-control"" type=""password"" />
            </div>

            <div class=""form-group"">
                <input type=""submit"" value=""Login"" class=""btn btn-success btn3d"" />
            </div>
        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1114, 752, true);
            WriteLiteral(@"
    </div>

    <div>
        <p>Para ingresar como <strong>Paciente</strong>      ==> user: <strong>paciente</strong> pass: <strong>Paciente123</strong></p>
        <p>Para ingresar como <strong>Profesional</strong>   ==> user: <strong>prof</strong> pass: <strong>Profesional123</strong></p>
        <p>Para ingresar como <strong>Administrador</strong> ==> user: <strong>admin</strong> pass: <strong>admin</strong></p>
        <br />
        <p>Para ingresar como <strong>Profesional (Marchese)</strong> ==> user: <strong>fmarchese</strong> pass: <strong>Marchese123</strong></p>
        <p>Para ingresar como <strong>Paciente (Capelli)</strong> ==> user: <strong>rcapelli</strong> pass: <strong>Capelli123</strong></p>

    </div>
</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
